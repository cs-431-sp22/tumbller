# Tumbller

## Project Setup

This project depends on the installation of the following Arduino packages:

 - Adafruit MPU6050 2.0.6
 - Kalman Filter Library 1.0.2
 - MsTimer2 1.1.0
 - PinChangeInterrupt 1.2.9
