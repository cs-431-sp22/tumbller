/*
 *  Copyright (C) 2022  University of Illinois Board of Trustees
 *
 *  Developed by:   Simon Yu (jundayu2@igt564/llinois.edu)
 *                  Department of Electrical and Computer Engineering
 *                  https://www.simonyu.net/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *  @file   tumbller.ino
 *  @author Simon Yu
 *  @date   01/12/2022
 *  @brief  Arduino program source.
 *
 *  This file contains object pointers, Arduino functions,
 *  interrupt handlers, as well as task functions.
 */

/*
 *  External headers.
 */
#include <PinChangeInterrupt.h>

/*
 *  Project headers.
 */
#include "src/actuator/actuator.h"
#include "src/controller/controller.h"
#include "src/utility/logger.h"
#include "src/planner/maneuver_planner.h"
#include "src/utility/math.h"
#include "src/neopixel/neopixel.h"
#include "src/common/pin.h"
#include "src/planner/planner.h"
#include "src/sensor/sensor.h"
#include "src/utility/timer.h"
#include "src/planner/waypoint_planner.h"
#include "src/planner/turn_planner.h"
#include "src/utility/bluetooth.h"
// #include "src/navigation/astar.cpp"

/*
 *  Use tumbller namespace.
 */
using namespace tumbller;

/*
 *  Object pointers.
 */
Actuator* actuator_ = nullptr;
Controller* controller_ = nullptr;
Logger* logger_ = nullptr;
NeoPixel* neopixel_ = nullptr;
Planner* planner_ = nullptr;
Sensor* sensor_ = nullptr;
Timer* timer_ = nullptr;
Bluetooth* bluetooth_ = nullptr;

/*
 *  Timing variables, all in seconds.
 */
float period_fast_domain_ = 0.005;
float period_slow_domain_ = 0.04;
float timer_domain_ = 0;

/**
 *  @brief  Left encoder interrupt handler.
 *
 *  This function is called when the left encoder
 *  interrupt is raised. The function calls the
 *  corresponding callback member function in
 *  the Sensor class.
 */
void encoderLeftInterruptHandler() {
    if (!sensor_) {
        return;
    }
    sensor_->onEncoderLeftChange();
}

/**
 *  @brief  Right encoder interrupt handler.
 *
 *  This function is called when the right encoder
 *  interrupt is raised. The function calls the
 *  corresponding callback member function in
 *  the Sensor class.
 */
void encoderRightInterruptHandler() {
    if (!sensor_) {
        return;
    }
    sensor_->onEncoderRightChange();
}

/**
 *  @brief  Ultrasound interrupt handler.
 *
 *  This function is called when the ultrasound
 *  interrupt is raised. The function calls the
 *  corresponding callback member function in
 *  the Sensor class.
 */
void ultrasoundInterruptHandler() {
    if (!sensor_) {
        return;
    }
    static bool rising = true;

    if (rising) {
        sensor_->onUltrasound(rising);

        rising = false;
        attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(
                Pin::ultrasound_pong), ultrasoundInterruptHandler, FALLING);
    } else {
        sensor_->onUltrasound(rising);

        rising = true;
        attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(
                Pin::ultrasound_pong), ultrasoundInterruptHandler, RISING);
    }
}

/**
 *  @brief  Real-time task function.
 *
 *  This function performs real-time tasks with
 *  strict timing deadlines. There are two timing
 *  domains, a fast domain and a slow domain. The
 *  function is called by the hardware timer
 *  interrupt handler.
 */
void realTimeTasks() {
    sensor_->sense(true);
    controller_->control(sensor_->getSensorData(), true);

    if (timer_domain_ >= period_slow_domain_) {
        sensor_->sense(false);
        controller_->control(sensor_->getSensorData(), false);

        timer_domain_ = 0;
    }

    actuator_->actuate(controller_->getActuationCommand());

    timer_domain_ += period_fast_domain_;
}

/**
 *  @brief  Best-effort task function.
 *
 *  This function performs best-effort or
 *  non-real-time tasks that do not require
 *  strict timing deadlines. The function is
 *  called by the Arduino loop function. The
 *  best-effort tasks are only run after the
 *  real-time tasks finish during each period.
 */
void bestEffortTasks() {
    digitalWrite(13, HIGH);
    bluetooth_->Data_Processing();

    planner_->plan();
    neopixel_->refresh();

    digitalWrite(13, LOW);
}

/**
 *  @brief  Arduino setup function.
 *
 *  This function creates, configures, and launches
 *  objects. The function also attaches the interrupt
 *  handlers to the corresponding pins.
 */
void setup() {
    logger_ = new Logger();
    actuator_ = new Actuator(logger_);
    controller_ = new Controller(logger_);
    neopixel_ = new NeoPixel(logger_, controller_);
    sensor_ = new Sensor(logger_, actuator_);
    planner_ = new TurnPlanner(logger_, sensor_, controller_);
    timer_ = new Timer(logger_);
    bluetooth_ = new Bluetooth(controller_, sensor_, planner_);

    controller_->setPeriod(period_fast_domain_, true);
    controller_->setPeriod(period_slow_domain_, false);
    sensor_->setPeriod(period_fast_domain_, true);
    sensor_->setPeriod(period_slow_domain_, false);
    timer_->setPeriod(period_fast_domain_);

    controller_->getPlanner(planner_);

    attachInterrupt(digitalPinToInterrupt(
            Pin::motor_left_encoder), encoderLeftInterruptHandler, CHANGE);

    attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(
            Pin::motor_right_encoder), encoderRightInterruptHandler, CHANGE);

    attachPinChangeInterrupt(digitalPinToPinChangeInterrupt(
            Pin::ultrasound_pong), ultrasoundInterruptHandler, RISING);

     timer_->enable();

     logger_->logInfo("Initialized");


   pinMode(13, OUTPUT);
}

/**
 *  @brief  Arduino loop function.
 *
 *  This function is periodically called by the
 *  Arduino framework. The function calls the
 *  best-effort task function.
 */
void loop() {
    bestEffortTasks();
}

/**
 *  @brief  Hardware timer 2 interrupt handler.
 *
 *  This function is periodically called whenever the
 *  hardware timer 2 overflows. The function calls the
 *  real-time task function. See the Timer class for
 *  the configuration of the hardware timer 2.
 */
ISR(TIMER2_COMPA_vect) {
    sei();
    realTimeTasks();
}
