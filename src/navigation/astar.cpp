// #ifndef CORDINATE_H
// #define CORDINATE_H
// // #include <stack>
// #include "Arduino.h"
// #include "stack.h"
// #include "node.h"
// #include "vector.h"
// #define X_MAX 4
// #define Y_MAX 4
// #define FLT_MAX 999999999

// inline bool operator < (const Node& lhs, const Node& rhs)
// {
// 	return lhs.fCost < rhs.fCost;
// }

// // World
// // 0 empty
// // 1 Wall
// // 2 Robot
// // 3 Goal



// class Cordinate {
// public:
// 	Cordinate(){}

// 	bool isValid(int x, int y, int world[X_MAX][Y_MAX]) { //If our Node is an obstacle it is not valid

// 		if (world[x][y] != 1) {
// 			if (x < 0 || y < 0 || x >= X_MAX || y >= Y_MAX) {
// 				return false;
// 			}
// 			return true;
// 		} 
// 		return false;
// 	}

// 	bool isDestination(int x, int y, int world[X_MAX][Y_MAX]) {
// 		if (world[x][y] == 3) {
// 			return true;
// 		}
// 		return false;
// 	}

// 	double calculateH(int x, int y, Node dest) {
// 		double H = ( (x - dest.x)*(x - dest.x) + (y - dest.y)*(y - dest.y) 	);
// 		return H;
// 	}

// 	Vector makePath(Node map[X_MAX][Y_MAX], Node dest) {
//         // cout << "Found a path" << endl;
//         int x = dest.x;
//         int y = dest.y;
//         Stack path;
//         Vector usablePath;

//         while (!(map[x][y].parentX == x && map[x][y].parentY == y)
//             && map[x][y].x != -1 && map[x][y].y != -1) 
//         {
//             path.push(map[x][y]);
//             int tempX = map[x][y].parentX;
//             int tempY = map[x][y].parentY;
//             x = tempX;
//             y = tempY;
            
//         }
//         path.push(map[x][y]);

//         while (!path.empty()) {
//             Node top = path.pop();
//             //cout << top.x << " " << top.y << endl;
//             usablePath.emplace_back(top);
//         }
//         return usablePath;
		
// 	}


// 	Vector aStar(Node player, Node dest, int world[X_MAX][Y_MAX]) {
// 		delay(100);
// 		Vector empty;
// 		if (isValid(dest.x, dest.y, world) == false) {
// 			// cout << "Destination is an obstacle" << endl;
// 			return empty;
// 			//Destination is invalid
// 		}
// 		if (isDestination(player.x, player.y, world)) {
// 			// cout << "You are the destination" << endl;
// 			return empty;
// 			//You clicked on yourself
// 		}
// 		bool closedList[(X_MAX)][(Y_MAX)];

// 		int count = 0;
// 		Serial.println(count++);

// 		//Initialize whole map
// 		Node allMap[X_MAX][Y_MAX];
// 		for (int x = 0; x < (X_MAX); x++) {
// 			for (int y = 0; y < (Y_MAX); y++) {
// 		Serial.println(count++);
// 				allMap[x][y].fCost = FLT_MAX;
// 				allMap[x][y].gCost = FLT_MAX;
// 				allMap[x][y].hCost = FLT_MAX;
// 				allMap[x][y].parentX = -1;
// 				allMap[x][y].parentY = -1;
// 				allMap[x][y].x = x;
// 				allMap[x][y].y = y;

// 				closedList[x][y] = false;
// 			}
// 		}

// 		Serial.println(6969);
		
// 		//Initialize our starting list
// 		int x = player.x;
// 		int y = player.y;
// 		allMap[x][y].fCost = 0.0;
// 		allMap[x][y].gCost = 0.0;
// 		allMap[x][y].hCost = 0.0;
// 		allMap[x][y].parentX = x;
// 		allMap[x][y].parentY = y;

// 		Vector openList;	
// 		openList.emplace_back(allMap[x][y]);
// 		bool destinationFound = false;

// 		while (!openList.empty() && openList.size() < X_MAX * Y_MAX) {
// 			Node node;
// 			do {
// 				//This do-while loop could be replaced with extracting the first
// 				//element from a set, but you'd have to make the openList a set.
// 				//To be completely honest, I don't remember the reason why I do
// 				//it with a vector, but for now it's still an option, although
// 				//not as good as a set performance wise.
// 				float cost = FLT_MAX;
//                 int idx;
//                 for (int i = 0; i < openList.size(); i++){
// 					Node n = openList.get(i);
// 					if (n.fCost < cost) {
// 						cost = n.fCost;
// 						idx = i;
// 					}

//                 }
// 				node = openList.get(idx);
// 				openList.erase(idx);
// 			} while (isValid(node.x, node.y, world) == false);

// 			x = node.x;
// 			y = node.y;
// 			closedList[x][y] = true;

// 			//For each neighbour starting from North-West to South-East
// 			int neighX[4] = {1, -1, 0, 0};
// 			int neighY[4] = {0, 0, 1 ,-1};

// 			for (int i = 0; i < 4; i++){
// 				int newX = neighX[i];
// 				int newY = neighY[i];
// 				double gNew, hNew, fNew;
// 				if (isValid(x + newX, y + newY, world)) {
// 					if (isDestination(x + newX, y + newY, world))
// 					{
// 						//Destination found - make path
// 						allMap[x + newX][y + newY].parentX = x;
// 						allMap[x + newX][y + newY].parentY = y;
// 						destinationFound = true;
// 						return makePath(allMap, dest);
// 					}
// 					else if (closedList[x + newX][y + newY] == false)
// 					{
// 						gNew = node.gCost + 1.0;
// 						hNew = calculateH(x + newX, y + newY, dest);
// 						fNew = gNew + hNew;
// 						// Check if this path is better than the one already present
// 						if (allMap[x + newX][y + newY].fCost == FLT_MAX ||
// 							allMap[x + newX][y + newY].fCost > fNew)
// 						{
// 							// Update the details of this neighbour node
// 							allMap[x + newX][y + newY].fCost = fNew;
// 							allMap[x + newX][y + newY].gCost = gNew;
// 							allMap[x + newX][y + newY].hCost = hNew;
// 							allMap[x + newX][y + newY].parentX = x;
// 							allMap[x + newX][y + newY].parentY = y;
// 							openList.emplace_back(allMap[x + newX][y + newY]);
// 						}
// 					}
// 				}
			
// 			}
// 		}
// 		if (destinationFound == false) {
// 			// cout << "Destination not found" << endl;
// 			return empty;
// 		}
// 		return empty;
// 	}
// };
// #endif
