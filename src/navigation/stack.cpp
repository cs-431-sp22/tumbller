// #ifndef STACK_CPP
// #define STACK_CPP


// #include "stack.h"

// //-------------------------------------------------
// // Implementation of Stack Class:
// //-------------------------------------------------
 
// // Stack Constructor function
// Stack::Stack(int MaxSize) :
//     MAX_NUM( MaxSize )    // Initialize the constant
// {
//     Data = new Node[MAX_NUM];
//     CurrElemNum = 0;
// }
 
// // Stack Destructor function
// Stack::~Stack(void)
// {
//     delete[] Data;
// }
 
// // Push() function
// void Stack::push(const Node &Item)
// {
//     // Error Check: Make sure we aren't exceeding the maximum storage space
//     assert(CurrElemNum < MAX_NUM);

//     Data[CurrElemNum++] = Item;
// }
 
// // Pop() function
// Node Stack::pop(void)
// {
//     // Error Check: Make sure we aren't popping from an empty Stack
//     assert(CurrElemNum > 0);

//     return Data[--CurrElemNum];
// }
 
// // Peek() function
// const Node &Stack::peek(int Depth) const
// {
//     // Error Check: Make sure the depth doesn't exceed the number of elements
//     assert(Depth < CurrElemNum);

//     return Data[ CurrElemNum - (Depth + 1) ];
// }

 
// // Pop() function
// bool Stack::empty(void)
// {
//     return CurrElemNum == 0;
// }

// #endif /*STACK_CPP*/
