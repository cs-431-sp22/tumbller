// /*
//      -------------------------------------------------------------------
//     |                                                                   |
//     |    Stack Class                                                    |
//     |    ===========================================================    |
//     |    This Stack has been implemented with templates to allow it     |
//     |    to accomodate virtually any data type, and the size of the     |
//     |    Stack is determined dynamically at runtime.                    |
//     |                                                                   |
//     |    There is also a new function: peek(), which, given a whole     |
//     |    number 'Depth', returns the Stack element which is 'Depth'     |
//     |    levels from the top.                                           |
//     |                                                                   |
//      -------------------------------------------------------------------
// */
 
// #ifndef STACK_H
// #define STACK_H
 
// #include <assert.h>    // For error-checking purposes
// #include "node.h"    
 
// //-------------------------------------------------
// // Main structure of Stack Class:
// //-------------------------------------------------
 
// class Stack
// {
//   public:
//     Stack(int MaxSize=10);
//     ~Stack(void);
 
//     void        push(const Node &Item); // Adds Item to the top
//     Node        pop(void);              // Returns Item from the top
//     const Node &peek(int Depth) const;  // Peek a depth downwards

//     bool empty();
 
//   protected:
//     Node     *Data;           // The actual Data array
//     int       CurrElemNum;    // The current number of elements
//     const int MAX_NUM;        // Maximum number of elements
// };

 
// #endif /*STACK_H*/
