/*
 *  Copyright (C) 2022  University of Illinois Board of Trustees
 *
 *  Developed by:   Simon Yu (jundayu2@illinois.edu)
 *                  Department of Electrical and Computer Engineering
 *                  https://www.simonyu.net/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *  @file   controller.cpp
 *  @author Simon Yu
 *  @date   01/13/2022
 *  @brief  Controller class source.
 *
 *  This file implements the controller class.
 */

/*
 *  Project headers.
 */
#include "../common/motor.h"
#include "../controller/controller.h"
#include "../utility/logger.h"
#include "../utility/math.h"
#include "../sensor/sensor.h"
#include "../sensor/sensor_data.h"

/*
 *  tumbller namespace.
 */
namespace tumbller
{
    
Controller::Controller(Logger* logger) : logger_(logger), controller_target_(), 
        pid_controller_position_x_(logger), pid_controller_attitude_y_(logger), 
        pid_controller_attitude_z_(logger), output_position_x_(0), 
        output_attitude_y_(0), output_attitude_z_(0), active_(false) {
    
    pid_controller_gain_position_x_.proportional = 960;
    pid_controller_gain_position_x_.differential = 1280;
    pid_controller_gain_position_x_.integral = 0;
    pid_controller_gain_position_x_.open_loop = 0;
    pid_controller_gain_position_x_.integral_max = 0;

    pid_controller_gain_attitude_y_.proportional = -3150;
    pid_controller_gain_attitude_y_.differential = -45;
    pid_controller_gain_attitude_y_.integral = 0;
    pid_controller_gain_attitude_y_.open_loop = 0;
    pid_controller_gain_attitude_y_.integral_max = 0;

    pid_controller_gain_attitude_z_.proportional = 0;
    pid_controller_gain_attitude_z_.differential = 30;
    pid_controller_gain_attitude_z_.integral = 100;
    pid_controller_gain_attitude_z_.open_loop = 100;
    pid_controller_gain_attitude_z_.integral_max = 0;

    pid_controller_position_x_.setGain(pid_controller_gain_position_x_);
    pid_controller_attitude_y_.setGain(pid_controller_gain_attitude_y_);
    pid_controller_attitude_z_.setGain(pid_controller_gain_attitude_z_);


    pid_controller_saturation_position_x_.saturation_input_lower = -0.3;
    pid_controller_saturation_position_x_.saturation_input_upper = 0.5;
    
    pitch_adjust_ = 0.023;
    newOrigin = 0;
    xPos = 0;
    turnRatio = 1.0;
    proxySensor = true;
    printUltraSound = false;

    pid_controller_position_x_.setSaturation(pid_controller_saturation_position_x_);
    pid_controller_attitude_y_.setSaturation(pid_controller_saturation_attitude_y_);
    pid_controller_attitude_z_.setSaturation(pid_controller_saturation_attitude_z_);

    setControllerTarget(controller_target_);
}

ActuationCommand Controller::getActuationCommand() const {
    return actuation_command_;
}

bool Controller::getActiveStatus() const {
    return active_;
}

ControllerTarget Controller::getControllerTarget() const {
    return controller_target_;
}

void Controller::setControllerTarget(const ControllerTarget& controller_target) {
    controller_target_ = controller_target;

    pid_controller_position_x_.setTarget(controller_target_.position_x);
    pid_controller_attitude_y_.setTarget(controller_target_.attitude_y);
    pid_controller_attitude_z_.setTarget(controller_target_.attitude_z);
}

void Controller::setPeriod(const float& period, const bool& fast_domain) {
    if (fast_domain) {
        pid_controller_attitude_y_.setPeriod(period);
    } else {
        pid_controller_position_x_.setPeriod(period);
        pid_controller_attitude_z_.setPeriod(period);
    }
}

void Controller::getPlanner(TurnPlanner* planner){
    planner_ = planner;
}

void Controller::enableProxy(bool status){
    proxySensor = status;
}

void Controller::printUltrasound(){
    printUltraSound = true;
}

void Controller::control(const SensorData& sensor_data, const bool& fast_domain) {

    if (fabs(radiansToDegrees(sensor_data.attitude_y)) >= 20) {
        active_ = false;
    } else {
        if (!active_) {
            pid_controller_position_x_.resetErrorIntegral();
            pid_controller_attitude_y_.resetErrorIntegral();
            pid_controller_attitude_z_.resetErrorIntegral();
        }
        active_ = true;
    }


    if (fast_domain) {
        pid_controller_attitude_y_.setState(sensor_data.attitude_y);
        pid_controller_attitude_y_.setErrorDifferential(sensor_data.angular_velocity_y - 0.05);

        output_attitude_y_ = pid_controller_attitude_y_.control();
    } else {

        xPos = sensor_data.position_x;

        if (printUltraSound){
            Serial.println(sensor_data.distance_ultrasound);
            printUltraSound = false;
        }

        if (proxySensor && sensor_data.distance_ultrasound <= 0.5 && sensor_data.position_x - newOrigin + 0.1 < controller_target_.position_x){
            newOrigin = xPos;
            controller_target_.position_x = 0;
            setControllerTarget(controller_target_);
            planner_->reset(true);
        }

        pid_controller_position_x_.setState(sensor_data.position_x - newOrigin + 0.15);
        pid_controller_position_x_.setErrorDifferential(sensor_data.velocity_x);

        pid_controller_attitude_z_.setOpenLoop(
                -1 * pid_controller_attitude_z_.getTarget() * fabs(sensor_data.velocity_x));
        pid_controller_attitude_z_.setErrorDifferential(sensor_data.angular_velocity_z);

        output_position_x_ = pid_controller_position_x_.control();
        output_attitude_z_ = pid_controller_attitude_z_.control();

    }


    float output_motor_left = output_position_x_ + output_attitude_y_ - output_attitude_z_ * turnRatio;
    float output_motor_right = output_position_x_ + output_attitude_y_ + output_attitude_z_ / turnRatio;

    if (!active_) {
        output_motor_left = 0;
        output_motor_right = 0;
    }


    actuation_command_.motor_enable = true;


    actuation_command_.motor_left_forward = output_motor_left >= static_cast<float>(
            Motor::pwm_min) ? true : false;
    actuation_command_.motor_right_forward = output_motor_right >= static_cast<float>(
            Motor::pwm_min) ? true : false;

    actuation_command_.motor_left_pwm = constrain(fabs(output_motor_left), static_cast<float>(
            Motor::pwm_min), static_cast<float>(Motor::pwm_max));
    actuation_command_.motor_right_pwm = constrain(fabs(output_motor_right), static_cast<float>(
            Motor::pwm_min), static_cast<float>(Motor::pwm_max));
}

void Controller::changeControl(int target, float amount) {
    switch(target){
        case 0:
            pid_controller_gain_attitude_y_.proportional += amount;
            pid_controller_attitude_y_.setGain(pid_controller_gain_attitude_y_);
            break;
        case 1:
            pid_controller_gain_attitude_y_.differential += amount;
            pid_controller_attitude_y_.setGain(pid_controller_gain_attitude_y_);
            break;
        case 2:
            pitch_adjust_ += amount;
            break;
        case 3:
            pid_controller_gain_position_x_.proportional += amount;
            pid_controller_position_x_.setGain(pid_controller_gain_position_x_);
            break;
        case 4:
            pid_controller_gain_position_x_.differential += amount;
            pid_controller_position_x_.setGain(pid_controller_gain_position_x_);
            break;
        case 5:
            newOrigin = xPos;
            break;
        case 6:
            turnRatio += amount;
            break;
        case 8:
            pid_controller_gain_attitude_z_.open_loop += amount;
            pid_controller_attitude_z_.setGain(pid_controller_gain_attitude_z_);
            break;
    }
}

}   // namespace tumbller
