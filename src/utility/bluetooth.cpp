/**
 *  @file   bluetooth.cpp
 *  @author Krzysztof Dutka
 *  @date   04/11/2022
 *  @brief  Bluetooth class source.
 *
 *  This file implements the bluetooth class.
 */
#include "bluetooth.h"


namespace tumbller
{

Bluetooth::Bluetooth(Controller* controller, Sensor* sensor,
                        TurnPlanner* planner) {
    controller_ = controller;
    controller_target_ = *(new ControllerTarget());

    sensor_ = sensor;
    planner_ = planner;
}


void Bluetooth::Data_Processing() {
    if (Serial.available()) {

        char data = Serial.read();

        if (data != '\0' && data != '\n') {
            switch (data) {

                case 'r': 
                    // Reset X position
                    controller_->changeControl(5, 0);
                    controller_target_.position_x = 0;
                    controller_->setControllerTarget(controller_target_);
                    planner_->reset(true);
                    break;
                case 'R': 
                    // Reset Z position
                    controller_->changeControl(8, 0);
                    controller_target_.attitude_z = 0;
                    controller_->setControllerTarget(controller_target_);
                    break;
                case 'w': 
                    // Move Forward
                    planner_->move(true);
                    break;
                case 'a': 
                    // Turn left
                    planner_->turn(true);
                    break;
                case 's': 
                    // Move back
                    planner_->move(false);
                    break;
                case 'd': 
                    // Turn right
                    planner_->turn(false);
                    break;
                case '1': 
                    // Lean Backwards
                    controller_->changeControl(2, 0.001);
                    break;
                case '2': 
                    // Lean forwards
                    controller_->changeControl(2, -0.001);
                    break;
                case '3': 
                    controller_->changeControl(6, 0.01);
                    break;
                case '4': 
                    controller_->changeControl(6, -0.01);
                    break;
                case '5': 
                    // Y Proportional weaker 
                    controller_->changeControl(0, 100);
                    break;
                case '6': 
                    // Y Proportional stronger 
                    controller_->changeControl(0, -100);
                    break;
                case '7': 
                    // Y differential weaker 
                    controller_->changeControl(1, 10);
                    break;
                case '8': 
                    // Y differential stronger 
                    controller_->changeControl(1, -10);
                    break;
                case '9': 
                    // X Proportional weaker 
                    controller_->changeControl(3, 100);
                    break;
                case '0': 
                    // X Proportional stronger 
                    controller_->changeControl(3, -100);
                    break;
                case 'z': 
                    // X differential weaker 
                    controller_->changeControl(4, 10);
                    break;
                case 'x': 
                    // X differential stronger 
                    controller_->changeControl(4, -10);
                    break;
                case 'c': 
                    // Longer turns
                    planner_->changeControl(0, 0.05);
                    break;
                case 'v': 
                    // Shorter Turns
                    planner_->changeControl(0, -0.05);
                    break;
                case 'b': 
                    // Farther Turns
                    planner_->changeControl(1, 0.05);
                    break;
                case 'n': 
                    // Closer Turns
                    planner_->changeControl(1, -0.05);
                    break;
                case 'N': 
                    // Disable builtin navigation
                    planner_->enableNav(false);
                    break;
                case 'M': 
                    // Print navi status
                    planner_->enableNav(true);
                    break;
                case 'X': 
                    // Print x position
                    planner_->getPosX();
                    break;
                case 'Y': 
                    // Print y position
                    planner_->getPosY();
                    break;
                case 'D': 
                    // Print direction
                    planner_->getDir();
                    break;
                case 'U': 
                    // Print direction
                    controller_->printUltrasound();
                    break;
                case 'S': 
                    // Print direction
                    planner_->getPlanStatus();
                    break;
                case 'p': 
                    // Ping
                    Serial.println("Ping");
                    break;
                case 'P': 
                    // Ping
                    Serial.println("Pong");
                    break;
            }
        }
    }
}


} // namespace tumbller




// If you want to receive strings
// Look below



// #include <Regexp.h>

// String inData;
// #define numChars 32




// recvWithStartEndMarkers();

// char data_array[inData.length() + 1];
// inData.toCharArray(data_array, inData.length() + 1);
// MatchState ms (data_array);

// if ( ms.Match("W%d%d%d") == REGEXP_MATCHED ) {
//     // Changes the light value of the LEDs
//     //  W123
//     white_val = inData.substring(1, 4).toInt();

//     inData = "";
// }




// //https://forum.arduino.cc/t/serial-input-basics-updated/382007/2
// void recvWithStartEndMarkers() {
//     char receivedChars[numChars];
//     boolean newData;
//     static boolean recvInProgress = false;
//     static byte ndx = 0;
//     char startMarker = '<';
//     char endMarker = '>';
//     char rc;

//     while (Serial.available() > 0 && newData == false) {
//         rc = Serial.read();

//         if (recvInProgress == true) {
//             if (rc != endMarker) {
//                 receivedChars[ndx] = rc;
//                 ndx++;
//                 if (ndx >= numChars) {
//                         ndx = numChars - 1;
//                 }
//             } else {
//                 receivedChars[ndx] = '\0'; // terminate the string
//                 recvInProgress = false;
//                 ndx = 0;
//                 newData = true;
//             }
//         } else if (rc == startMarker) {
//             recvInProgress = true;
//         }
//     }
//     if (newData == true) {
//         inData = String(receivedChars);
//         newData = false;
//     }
// }
