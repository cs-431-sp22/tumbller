/**
 *  @file   bluetooth.h
 *  @author Krzysztof Dutka
 *  @date   04/11/2022
 *  @brief  Bluetooth class source.
 *
 *  This file defines the bluetooth class.
 */

#ifndef SRC_BLUETOOTH_DATA_H_
#define SRC_BLUETOOTH_DATA_H_


#include <Arduino.h>
#include "../utility/logger.h"
#include "../controller/controller.h"
#include "../sensor/sensor.h"
#include "../planner/planner.h"
#include "../planner/turn_planner.h"

namespace tumbller
{

class Bluetooth {
    public:

        Bluetooth(Controller* controller, Sensor* sensor, TurnPlanner* planner);

        void
        Data_Processing();

    private:

        Controller* controller_;
        ControllerTarget controller_target_;

        Sensor* sensor_;
        TurnPlanner* planner_;
};

} // namespace tumbller

#endif /* SRC_BLUETOOTH_DATA_H_ */