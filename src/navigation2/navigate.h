#ifndef NAVIGATE_H_
#define NAVIGATE_H_

// #include "../actuator/actuator_data.h"

namespace tumbller
{

    enum Direction : int
    {
        North,
        East,
        South,
        West
        
    };

    enum Moves : int
    {
        Forward,
        Right,
        Left,
        Backwards,
        Stay,
        Error
    };

    class Navigater {
        public:

            Navigater(float destX, float destY);

            void move(float amount);
            void turn(bool left);
            int newMove(float ultrasound);

            void getPosX();
            void getPosY();
            void getDir();
            
            void reset();

        private:

            Direction dir_;

            float xpos_;
            float ypos_;

            float destX_;
            float destY_;
            
    };
}   // namespace tumbller

#endif  // NAVIGATE_H_
