#include "../navigation2/navigate.h"
#include <Arduino.h>

namespace tumbller
{
    
    Navigater::Navigater(float destX, float destY) {

        xpos_ = 0;
        ypos_ = 0;

        destX_ = destX;
        destY_ = destY;
        
        dir_ = Direction::East;
    }

    void Navigater::move(float amount) {
        switch (dir_) {
            case Direction::North:
                ypos_ -= amount;
                break;
            case Direction::East:
                xpos_ += amount;
                break;
            case Direction::South:
                ypos_ += amount;
                break;
            case Direction::West:
                xpos_ -= amount;
                break;
            default:
                break;
        }
    }

    void Navigater::turn(bool left) {
        switch (dir_) {
            case Direction::North:
                if (left)
                    dir_ = Direction::West;
                else
                    dir_ = Direction::East;
                break;
            case Direction::East:
                if (left)
                    dir_ = Direction::North;
                else
                    dir_ = Direction::South;
                break;
            case Direction::South:
                if (left)
                    dir_ = Direction::East;
                else
                    dir_ = Direction::West;
                break;
            case Direction::West:
                if (left)
                    dir_ = Direction::South;
                else
                    dir_ = Direction::North;
                break;
            default:
                break;
        }
    }

    int Navigater::newMove(float ultrasound) {
        /* 
            Moves::Forward,
            Moves::Right,
            Moves::Left,
            Moves::Backwards,
            Moves::Stay,
            Moves::Error
        */

        float eps = 0.3;
        float clearance = 0.5;

        switch (dir_) {
            case Direction::North:
                if (ypos_ - destY_ > eps && ultrasound > clearance) // South of dest
                    return Moves::Forward;
                if (ypos_ - destY_ < -eps) // North of dest
                    return Moves::Backwards;
                if (xpos_ - destX_ < -eps) // West of dest
                    return Moves::Right;
                if (xpos_ - destX_ > eps) // East of dest
                    return Moves::Left;
                return Moves::Stay;
                break;

            case Direction::East:
                if (xpos_ - destX_ < -eps && ultrasound > clearance) // West of dest
                    return Moves::Forward;
                if (xpos_ - destX_ > eps) // East of dest
                    return Moves::Backwards;
                if (ypos_ - destY_ < -eps) // North of dest
                    return Moves::Right;
                if (ypos_ - destY_ > eps) // South of dest
                    return Moves::Left;
                return Moves::Stay;
                break;

            case Direction::South:
                if (ypos_ - destY_ < -eps && ultrasound > clearance) // North of dest
                    return Moves::Forward;
                if (ypos_ - destY_ > eps) // South of dest
                    return Moves::Backwards;
                if (xpos_ - destX_ > eps) // East of dest
                    return Moves::Right;
                if (xpos_ - destX_ < -eps) // West of dest
                    return Moves::Left;
                return Moves::Stay;
                break;

            case Direction::West:
                if (xpos_ - destX_ > eps && ultrasound > clearance) // East of dest
                    return Moves::Forward;
                if (xpos_ - destX_ < -eps) // West of dest
                    return Moves::Backwards;
                if (ypos_ - destY_ > eps) // South of dest
                    return Moves::Right;
                if (ypos_ - destY_ < -eps) // North of dest
                    return Moves::Left;
                return Moves::Stay;
                break;
        }
        return Moves::Error; // Error
    }

    void Navigater::getPosX(){
        Serial.println(xpos_, 2);
    }
    
    void Navigater::getPosY(){
        Serial.println(ypos_, 2);
    }
    
    void Navigater::getDir(){
        Serial.println(dir_);
    }

    void Navigater::reset(){
        xpos_ = 0;
        ypos_ = 0;
        
        destX_ = 0;
        destY_ = 0;
    }

}   // namespace tumbller
