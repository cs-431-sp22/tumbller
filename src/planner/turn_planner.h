/*
 *  Copyright (C) 2022  University of Illinois Board of Trustees
 *
 *  Developed by:   Simon Yu (jundayu2@illinois.edu)
 *                  Department of Electrical and Computer Engineering
 *                  https://www.simonyu.net/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *  @file   turn_planner.h
 *  @author Simon Yu
 *  @date   01/20/2022
 *  @brief  Turn planner class header.
 *
 *  This file defines the turn planner class.
 */

/*
 *  Include guard.
 */
#ifndef SRC_PLANNER_TURN_PLANNER_H_
#define SRC_PLANNER_TURN_PLANNER_H_

/*
 *  Project headers.
 */
#include "../planner/planner.h"
#include "../planner/planner_data.h"
#include "../navigation2/navigate.h"

/*
 *  tumbller namespace.
 */
namespace tumbller
{
/*
 *  Forward declaration.
 */
class Controller;
class Logger;
class Sensor;
struct Turn;

/**
 *  @brief  Turn planner class.
 *
 *  This class provides functions for creating and
 *  executing a turn-based plan. The class
 *  inherits the planner abstract class.
 */
class TurnPlanner : public Planner
{
public:

    /**
     *  @param  logger Logger object pointer.
     *  @param  sensor Sensor object pointer.
     *  @param  controller Controller object pointer.
     *  @brief  Turn planner class constructor.
     *
     *  This constructor initializes all class member variables.
     *  Addtionally, the constructor defines a turn-based plan.
     */
    TurnPlanner(Logger* logger, Sensor* sensor, Controller* controller);

    /**
     *  @brief  Execute the defined turn-based plan.
     *
     *  This function executes a turn-based plan
     *  defined by the constructor. The function implements its
     *  pure abstract parent in the planner abstract class.
     *  This function is expected to be called periodically.
     */
    void
    plan() override;

    void
    turn(bool rightTurn);

    void
    move(bool forwards);

    void
    reset(bool fullReset);

    void
    changeControl(int target, float amount);

    void
    enableNav(bool enable);

    void
    getPosX();

    void
    getPosY();

    void
    getDir();

    void
    getPlanStatus();

private:

    Logger* logger_;    //!< Logger object pointer.
    Sensor* sensor_;    //!< Sensor object pointer.
    Controller* controller_;    //!< Controller object pointer.
    Waypoint* turn_;    //!< Current turn pointer.
    int turn_counter_;  //!< Turn counter.
    unsigned long turn_timer_;  //!< Turn timer, in seconds.
    volatile bool controller_active_;   //!< Controller active flag.
    volatile bool plan_started_;    //!< Plan started flag.
    volatile bool turn_started_;    //!< Turn started flag.
    volatile bool plan_completed_;  //!< Plan completed flag.

    float x_pos = 0.0;
    float turnTime = 2.0;
    float turnDist = 0.4;

    Waypoint* turn_1;
    Waypoint* turn_2;
    Waypoint* turn_3;
    Waypoint* turn_4;

    Navigater* navi_;
    bool enableNav_;
};
}   // namespace tumbller

#endif  // SRC_PLANNER_WAYPOINT_PLANNER_H_