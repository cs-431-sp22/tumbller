/*
 *  Copyright (C) 2022  University of Illinois Board of Trustees
 *
 *  Developed by:   Simon Yu (jundayu2@illinois.edu)
 *                  Department of Electrical and Computer Engineering
 *                  https://www.simonyu.net/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *  @file   turn_planner.cpp
 *  @author Simon Yu
 *  @date   01/20/2022
 *  @brief  Turn planner class source.
 *
 *  This file implements the turn planner class.
 */

/*
 *  Project headers.
 */
#include "../controller/controller.h"
#include "../utility/logger.h"
#include "../utility/math.h"
#include "../planner/planner_data.h"
#include "../sensor/sensor.h"
#include "../planner/turn_planner.h"

/*
 *  tumbller namespace.
 */
namespace tumbller
{
TurnPlanner::TurnPlanner(Logger* logger, Sensor* sensor, Controller* controller) : logger_(
        logger), sensor_(sensor), controller_(controller), turn_counter_(
        1), turn_timer_(0), controller_active_(
        false), plan_started_(false), turn_started_(
        false), plan_completed_(false) 
{
    navi_ = new Navigater(2, 1);
    enableNav_ = true;

    turn_1 = new Waypoint();
    turn_2 = new Waypoint();
    turn_3 = new Waypoint();
    turn_4 = new Waypoint();

    turn_1->controller_target.position_x = 0;
    turn_1->controller_target.attitude_z = degreesToRadians(0);
    turn_1->duration = 1;
    turn_1->next = nullptr;


    /*
     *  Set the current turn to turn 1 of the example plan.
     */
    turn_ = turn_1;
}


void
TurnPlanner::move(bool forwards)
{

    controller_->enableProxy(true);

    float moveDist = 0.5;
    if (forwards){
        // Serial.println("Moving forwards");   
        x_pos += moveDist;

        turn_1->controller_target.position_x = x_pos;
        turn_1->controller_target.attitude_z = degreesToRadians(0);
        turn_1->duration = 2;
        turn_1->next = turn_2;

        turn_2->controller_target.position_x = x_pos;
        turn_2->controller_target.attitude_z = degreesToRadians(0);
        turn_2->duration = 0.1;
        turn_2->next = nullptr;

        turn_ = turn_1;
        plan_completed_ = false;
        navi_->move(moveDist);
    } else {
        // Serial.println("Moving back");    
        x_pos -= moveDist;
        

        turn_1->controller_target.position_x = x_pos;
        turn_1->controller_target.attitude_z = degreesToRadians(0);
        turn_1->duration = 2;
        turn_1->next = turn_2;

        turn_2->controller_target.position_x = x_pos;
        turn_2->controller_target.attitude_z = degreesToRadians(0);
        turn_2->duration = 0.1;
        turn_2->next = nullptr;

        turn_ = turn_1;
        plan_completed_ = false;
        navi_->move(-moveDist);
    }
}


void
TurnPlanner::turn(bool leftTurn)
{
    controller_->enableProxy(false);

    x_pos -= turnDist / 2;

    turn_1->controller_target.position_x = x_pos;
    turn_1->controller_target.attitude_z = degreesToRadians(0);
    turn_1->duration = turnTime;
    turn_1->next = turn_2;

    if (!leftTurn){
        // Serial.println("Turning Right");
        x_pos += turnDist;

        turn_2->controller_target.position_x = x_pos;
        turn_2->controller_target.attitude_z = degreesToRadians(180);
        turn_2->duration = turnTime;
        turn_2->next = turn_3;
        navi_->turn(false);
    } else {
        // Serial.println("Turning Left");   
        x_pos += turnDist;

        turn_2->controller_target.position_x = x_pos;
        turn_2->controller_target.attitude_z = degreesToRadians(-180);
        turn_2->duration = turnTime;
        turn_2->next = turn_3;
        navi_->turn(true);
    }

    x_pos -= turnDist;

    turn_3->controller_target.position_x = x_pos;
    turn_3->controller_target.attitude_z = degreesToRadians(0);
    turn_3->duration = 2;
    turn_3->next = turn_4;

    turn_4->controller_target.position_x = x_pos;
    turn_4->controller_target.attitude_z = degreesToRadians(0);
    turn_4->duration = 0.1;
    turn_4->next = nullptr;

    turn_ = turn_1;
    plan_completed_ = false;

    controller_->enableProxy(true);
}

void
TurnPlanner::reset(bool fullReset)
{
    if (fullReset || turn_->controller_target.position_x < x_pos){
        x_pos = 0;

        turn_1->controller_target.position_x = x_pos;
        turn_1->controller_target.attitude_z = degreesToRadians(0);
        turn_1->duration = 0.1;
        turn_1->next = nullptr;

        turn_ = turn_1;
        plan_completed_ = false;
        
        // navi_->reset();
    }
}

void
TurnPlanner::changeControl(int target, float amount)
{
    switch(target){
        case 0:
            turnTime += amount;
            break;
        case 1:
            turnDist += amount;
            break;
    }
}

void
TurnPlanner::enableNav(bool enable)
{
    enableNav_ = enable;
}

void
TurnPlanner::getPosX()
{
    navi_->getPosX();
}

void
TurnPlanner::getPosY()
{
    navi_->getPosY();
}

void
TurnPlanner::getDir()
{
    navi_->getDir();
}

void
TurnPlanner::getPlanStatus()
{
    Serial.println(plan_completed_);
}

void
TurnPlanner::plan()
{
    /*
     *  Return if plan has been completed.
     */
    if (plan_completed_)
    {

        if (enableNav_)
            return;
        
        int newMove = navi_->newMove(sensor_->getSensorData().distance_ultrasound);

        if (newMove == Moves::Forward)
            move(true);
        if (newMove == Moves::Backwards)
            move(false);
        if (newMove == Moves::Right)
            turn(false);
        if (newMove == Moves::Left)
            turn(true);
        return;
    }

    /*
     *  Validate controller object pointer.
     */
    if (!controller_)
    {
        // logger_->logError("Controller missing");
        return;
    }

    /*
     *  Mark the plan as completed if the plan has started,
     *  has not completed, but the current turn is null.
     */
    if (!plan_completed_ && plan_started_ && !turn_)
    {
        // logger_->logInfo("Completed turn-based plan");
        plan_started_ = false;
        plan_completed_ = true;
        return;
    }

    /*
     *  Return if the controller is not active
     *  (pause the plan during safety disengage.)
     */
    if (!controller_active_)
    {
        controller_active_ = controller_->getActiveStatus();
        return;
    }

    if (!plan_started_)
    {
        /*
         *  The plan is empty if the plan has not started
         *  but the current turn is already null.
         */
        if (!turn_)
        {
            // logger_->logError("Empty turn-based plan");
            return;
        }

        /*
         *  Mark the plan as started if it has not started.
         */
        // logger_->logInfo("Started turn-based plan");
        plan_started_ = true;
    }

    if (!turn_started_)
    {
        /*
         *  Execute the current turn if it has not started.
         *  Set the controller target from the current turn,
         *  mark the current time, and mark the current turn
         *  as started.
         */
        // logger_->logInfo("Started turn " + String(turn_counter_));
        controller_->setControllerTarget(turn_->controller_target);
        turn_timer_ = millis();
        turn_started_ = true;
    }
    else
    {
        /*
         *  If the elapsed duration goes above the duration from the current
         *  turn, transtion to the next turn, increment the turn
         *  counter, and mark the current turn as not started.
         */
        if (static_cast<float>(millis() - turn_timer_) >=
                turn_->getDurationMillisecond())
        {
            turn_ = turn_->next;
            turn_counter_ ++;
            turn_started_ = false;
        }
    }
}
}   // namespace tumbller