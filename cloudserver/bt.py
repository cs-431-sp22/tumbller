# https://tutorialedge.net/python/concurrency/asyncio-event-loops-tutorial/
import asyncio
from typing import Callable, Any

from aioconsole import ainput
from bleak import BleakClient, discover

selected_device = []

class Connection:
    
    client: BleakClient = None
    
    def __init__(
        self,
        loop: asyncio.AbstractEventLoop,
        read_characteristic: str,
        write_characteristic: str,
        writeQueue=None, 
        readQueue=None
    ):
        self.loop = loop
        self.read_characteristic = read_characteristic
        self.write_characteristic = write_characteristic
        
        self.connected = False
        self.connected_device = None

        self.writeQueue = writeQueue
        self.readQueue = readQueue

    def on_disconnect(self, client: BleakClient, future: asyncio.Future):
        self.connected = False
        # Put code here to handle what happens on disconnect.
        print(f"Disconnected from {self.connected_device.name}!")

    async def cleanup(self):
        if self.client:
            await self.client.stop_notify(read_characteristic)
            await self.client.disconnect()

    async def manager(self):
        print("Starting connection manager.")
        while True:
            if self.client:
                await self.connect()
            else:
                await self.select_device()
                await asyncio.sleep(15.0)       

    async def connect(self):
        if self.connected:
            return
        try:
            await self.client.connect()
            self.connected = await self.client.is_connected()
            if self.connected:
                print(F"Connected to {self.connected_device.name}")
                self.client.set_disconnected_callback(self.on_disconnect)
                await self.client.start_notify(
                    self.read_characteristic, self.notification_handler,
                )
                while True:
                    if not self.connected:
                        break
                    await asyncio.sleep(3.0)
            else:
                print(f"Failed to connect to {self.connected_device.name}")
        except Exception as e:
            print(e)

    async def select_device(self):
        print("Bluetooth LE hardware warming up...")
        await asyncio.sleep(2.0) # Wait for BLE to initialize.
        devices = await discover()

        print("Please select device: ")
        for i, device in enumerate(devices):
            print(f"{i}: {device.address}  {device.name}")

        response = -1
        while True:
            if self.writeQueue == None:
                response = await ainput("Select device: ")  
            else: 
                print("Select device: ", end='', flush=True)
                response = self.writeQueue.get()

            try:
                response = int(response.strip())
            except:
                print("Please make valid selection.")
            
            if response > -1 and response < len(devices):
                break
            else:
                print("Please make valid selection.")

        print(f"Connecting to {devices[response].name}")
        self.connected_device = devices[response]
        self.client = BleakClient(devices[response].address, loop=self.loop)

    def notification_handler(self, sender: str, data: Any):
        # Where the data is received
        if self.readQueue == None:
            print("Received:", bytes(data).decode()[:-2], sep='')
        else:
            # print("Received:", bytes(data).decode()[:-2], sep='')
            self.readQueue.put(bytes(data).decode())

#############
# Loops
#############
async def user_console_manager(connection: Connection, queue=None):
    while True:
        if connection.client and connection.connected:

            if queue == None:
                input_str = await ainput("Enter string: ")
            else:
                try:
                    input_str = queue.get(timeout=0.1)
                except:
                    await asyncio.sleep(0.1)
                    continue
            # print(input_str)
            bytes_to_send = bytearray(map(ord, input_str))
            await connection.client.write_gatt_char(write_characteristic, bytes_to_send)
            # print(f"Sent: {input_str}")
        else:
            await asyncio.sleep(2.0)


async def main():
    while True:
        # YOUR APP CODE WOULD GO HERE.
        await asyncio.sleep(5)


def btMain(writeQueue=None, readQueue=None):
    # Create the event loop.
    # loop = asyncio.get_event_loop()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    connection = Connection(
        loop, read_characteristic, write_characteristic, writeQueue, readQueue
    )
    try:
        asyncio.ensure_future(connection.manager())
        asyncio.ensure_future(user_console_manager(connection, writeQueue))
        asyncio.ensure_future(main())
        loop.run_forever()
    except KeyboardInterrupt:
        print()
        print("User stopped program.")
    finally:
        print("Disconnecting...")
        loop.run_until_complete(connection.cleanup())


#############
# App Main
#############
read_characteristic = "0000ffe1-0000-1000-8000-00805f9b34fb"
write_characteristic = "0000ffe2-0000-1000-8000-00805f9b34fb"

if __name__ == "__main__":
    btMain()

