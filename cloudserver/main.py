from math import ceil
import queue
import threading
from bt import btMain
from astar import astar
import numpy as np

mapSize = 20
myMap = np.zeros((mapSize, mapSize))

endX = 0
endY = mapSize // 2

ultrasoundFactor = 1.0

DUMMYVALUE = '~'

def getNextMove(readQ):
    global myMap

    # Get X pos
    writeQ.put('X') 
    xpos = readQ.get()

    # Get Y pos
    writeQ.put('Y') 
    ypos = readQ.get()

    # Get direction
    writeQ.put('D') 
    dir = readQ.get()

    # Get ultrasound reading
    writeQ.put('U') 
    ultrasound = readQ.get()

    # Get car pos relative to left center
    xpos = float(xpos) * 2
    ypos = float(ypos) * 2 + mapSize // 2

    dir = int(dir)
    ultrasound = ceil(float(ultrasound) * 2 * ultrasoundFactor)

    # print('Readings', xpos, '|', ypos, '|', ultrasound, '|', dir)

    startPos = (int(xpos), int(ypos))
    endPos = (endX, endY)
    if ultrasound <= 3:
        # Update map only if ultrasound is within certain distance
        updateMap(startPos, ultrasound, dir)

    path = astar(myMap.tolist(), startPos, endPos, dir)

    if path == None:
        # No path found, clear map
        myMap = np.zeros((mapSize, mapSize))
        return DUMMYVALUE # Return dummy value

    if len(path) == 1:
        # Reached Destination
        return DUMMYVALUE # Return dummy value
    return calcDir(startPos, path[1], dir)

def updateMap(startPos, ultrasound, dir):
    global myMap

    pos = np.array(startPos)

    if dir == 0: # North
        shift = np.array((0, -1))
    elif dir == 1: # East
        shift = np.array((1, 0))
    elif dir == 2: # South
        shift = np.array((0, 1))
    elif dir == 3: # West
        shift = np.array((-1, 0))

    pos = pos + shift * ultrasound
    

    map[pos[0], pos[1]] = 1

def calcDir(startPos, nextPos, dir):
    moveDir = 0
    if startPos[0] > nextPos[0]:
        moveDir = 3 # West
    elif startPos[0] < nextPos[0]:
        moveDir = 1 # East
    elif startPos[1] > nextPos[1]:
        moveDir = 0 # North
    elif startPos[1] < nextPos[1]:
        moveDir = 2 # South
    

    # make moveDir relative
    moveDir = (moveDir - dir) % 4

    if moveDir == 0:
        return 'w'
    if moveDir == 1:
        return 'd'
    if moveDir == 2:
        return 'a' # Don't move backwards, no radar
    if moveDir == 3:
        return 'a'
    return DUMMYVALUE

def runNavi(writeQ, readQ):
    
    while True:
        writeQ.put('S') # Check if current move has completed
        try:
            if int(readQ.get()) == 1: # Move is completed
                writeQ.put(getNextMove(readQ)) # Give next Move
        except:
            print("Status: Unknown, clearing read queue")
            try:
                while True:
                    readQ.get(timeout=0.25)
            except:
                pass

if __name__ == "__main__":
    global myMap

    writeQ = None
    readQ = None
    writeQ = queue.Queue()
    readQ = queue.Queue()

    threading.Thread(target=btMain, args=(writeQ, readQ), daemon=True).start()

    if writeQ == None or readQ == None:
        while True:
            pass

    writeQ.put(input())
    input("Press enter when connected")

    threading.Thread(target=runNavi, args=(writeQ, readQ), daemon=True).start()

    tempX = endX
    tempY = endY

    while True:
        cmd = input("Enter command: ")
        if cmd == 'x':
            tempX = input("Enter new X destination: ")
        elif cmd == 'y':
            tempY = input("Enter new Y destination: ")
        elif cmd == 'r':
            endX = int(tempX)
            endY = int(tempY)
        elif cmd == 'c':
            break
        elif cmd == 'm':
            print(myMap)
        else:
            print("Enter \'x\' to enter new X destination")
            print("Enter \'y\' to enter new Y destination")
            print("Enter \'r\' to push new destination")
            print("Enter \'c\' to quit")
            
        

